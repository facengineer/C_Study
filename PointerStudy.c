#include<stdio.h>
#include<stdlib.h>
typedef struct TEST{
	int x;
	int y;
}TEST, *TEST_P;//typedef TEST *list; (Equal)

typedef struct NODE{
	int node_id;
	struct NODE *next;
}NODE, *NODE_LIST;

int main(int argc,char * argv[])
{
	int *value = (int *)malloc(sizeof(int));
	*value = 100;
	printf("value is: %d sizeof value is: %lu\n",*value,sizeof(*value));
	/**********************************************************************/
	TEST_P li = (TEST_P)malloc(sizeof(TEST));
	(*li).x = 1;
	(*li).y = 2;
	printf("number is: %d, weight is: %d size is: %lu\n",(*li).x,(*li).y,sizeof(li));
	/**********************************************************************/
	NODE *node_a = (NODE*)malloc(sizeof(NODE));
	NODE *node_b = (NODE*)malloc(sizeof(NODE));
	NODE *node_c = (NODE*)malloc(sizeof(NODE));
	
	node_a->node_id = 0;
	node_b->node_id = 1;
	node_c->node_id = 2;

	node_a->next = node_b;
	node_b->next = node_c;
	node_c->next = NULL;
	printf("node_a id is: %d ,next node id is: %d\n" , node_a->node_id, node_a->next->node_id);
	printf("ndoe_a size is: %lu\n",sizeof(*node_a));
	/**********************************************************************/
	int mos = 1;
	NODE_LIST list = (NODE_LIST)malloc(sizeof(NODE));
	NODE *list_pointer = list;
	for(mos;mos<10;mos++){
		NODE *tmp=(NODE *)malloc(sizeof(NODE));
		tmp->node_id = mos;
		list_pointer->next = tmp;
		list_pointer = tmp;
	}
	/**********************************************************************/
	while((list = list->next)!=NULL){
		printf("node_id is: %d\n",list->node_id);
	}
	/**********************************************************************/
	return(0);
}
